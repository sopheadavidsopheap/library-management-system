<?php
use Illuminate\Support\Facades\Route;

// Route::get('/', function () {
//     return view('welcome');
// });
Auth::routes();

Route::group([
    'namespace'=>'Frontend',
],function(){
    Route::get('/','ListBookController@index');
});
Route::group([
    'prefix'=>'admin',
    'namespace'=>'Admin',
    'middleware'=>'auth',

],function(){
    Route::get('/users', 'UserController@index')->name('admin.user.index');
});

Route::resource('/book', 'BookController');