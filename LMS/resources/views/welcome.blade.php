@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row show-grid">
        @foreach ($data as $item)
            <div class="col-lg-2 col-xs-12" style="margin:2px">
                <img src="{{URL::to('/')}}/images/{{$item->image}}" alt="alert" class="img-responsive model_img" id="sa-basic" style="width:100%;height:160px">
                <div>
                    <h3 class="box-title">{{$item->name}}</h3>
                    <small >{{$item->auth}}</small>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
