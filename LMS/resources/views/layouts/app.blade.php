 @include('layouts.header.head')
 @include('layouts.sidebar.sidebar')
    <main class="py-4">
        @yield('content')
    </main>
@include('layouts.footer.bottom')
