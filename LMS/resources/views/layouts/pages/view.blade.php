@extends('layouts.app')

@section('content')
<div class="container">
	<div style="text-align:right; padding:10px 0px">
			<a href="{{ route('book.index') }}" class="btn btn-primary">Back</a>
		</div>
		<div class="jumbotron text-center">
			<div class="row">
				<div class="col-5">
					<img src="{{ URL::to('/') }}/images/{{ $data->image }}" class="img-fluid rounded"/>
				</div>
				<div class="col-7">
					<div style="text-align:left">
						<h3>First Name - {{ $data->name }} </h3>
						<h3>Last Name - {{ $data->auth }}</h3>
					</div>
				</div>
			</div>
		</div>
</div>
@endsection
