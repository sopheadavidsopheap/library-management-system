@extends('layouts.app')

@section('content')
<div align="right">
	<a href="{{ route('book.index') }}" class="btn btn-sm btn-primary">Back</a>
</div><br/>
<form method="post" action="{{ route('book.update', $data->id) }}" enctype="multipart/form-data">
	@csrf
	@method('PATCH')
	<div class="row">
		<div class="col-5">
			<div class="col-md-8">
				<img src="{{ URL::to('/') }}/images/{{ $data->image }}" class="img-fluid rounded"/>
				<input type="hidden" name="hidden_image" value="{{ $data->image }}" />
				<input type="file" name="image" class="btn btn-light"/>
			</div>
		</div>
		<div class="col-7">
			<div style="text-align:left">
				<label for="">Name book</label><br/>
				<input class="form-control" name ="name" value="{{$data->name }}"/><br/>
				<label for="">Auth book</label><br/>
				<input class="form-control" name ="auth" value="{{$data->auth }}"/>
			</div>
			<input type="submit" name="edit" class="btn btn-info input-lg mt-3" value="Edit" />
		</div>
	</div>
</form>
@endsection



