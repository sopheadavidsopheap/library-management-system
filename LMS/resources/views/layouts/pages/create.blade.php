@extends('layouts.app')
@section('content')
<div class="container">
@if($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
	@endif
	<div align="right">
		<a href="{{ route('book.index') }}" class="btn btn-sm btn-primary">Back</a>
	</div><br/>
	<form method="post" action="{{ route('book.store') }}" enctype="multipart/form-data">
		@csrf
		<div class="row">
			<div class="col">
				<div class="col-md-8">
					<input type="file" name="image" class="btn btn-light"/>
					{{-- <input type="text" name="image" class="form-control input-lg"> --}}
				</div>
			</div>
			<div class="col">
				<div style="text-align:left">
					<label for="">Name book</label><br/>
					<input type="text" name="name" class="form-control input-lg" />
					<label for="">Auth book</label><br/>
					<input type="text" name="auth" class="form-control input-lg" />
				</div>
				<input type="submit" name="add" class="btn btn-primary input-lg" value="Add" />
			</div>
		</div>
	</form>

@endsection



