@include('layouts.footer.footer')
<!-- Bootstrap agency JavaScript -->
{{-- <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>
<script src="{{asset('s/jqBootstrapValidation.js')}}"></script>
<script src="{{asset('js/contact_me.js')}}"></script>
<script src="{{asset('js/agency.min.js')}}"></script> --}}

{{-- Bootstrap Admin JavaScript --}}
<script src="{{asset('plugins/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js')}}"></script>
<script src="{{asset('js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('js/waves.js')}}"></script>
<script src="{{asset('plugins/bower_components/waypoints/lib/jquery.waypoints.js')}}"></script>
<script src="{{asset('plugins/bower_components/counterup/jquery.counterup.min.js')}}"></script>
<script src="{{asset('plugins/bower_components/raphael/raphael-min.js')}}"></script>
<script src="{{asset('plugins/bower_components/morrisjs/morris.js')}}"></script>
<script src="{{asset('plugins/bower_components/chartist-js/dist/chartist.min.js')}}"></script>
<script src="{{asset('plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js')}}"></script>
<script src="{{asset('plugins/bower_components/moment/moment.js')}}"></script>
<script src="{{asset('plugins/bower_components/calendar/dist/fullcalendar.min.js')}}"></script>
<script src="{{asset('plugins/bower_components/calendar/dist/cal-init.js')}}"></script>
<script src="{{asset('js/custom.min.js')}}"></script>
<script src="{{asset('js/dashboard1.js')}}"></script>
<script src="{{asset('js/cbpFWTabs.js')}}"></script>
<script src="{{asset('text/javascript"')}}"></script>
</body></html>