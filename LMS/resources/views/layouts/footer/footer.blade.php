<footer class="footer">
  <div class="container">
      <div class="row">
          <div class="col-md-4">
              <span class="copyright">Copyright &copy; Your Website 2019</span>
          </div>
          <div class="col-md-4">
              <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                  <a href="#" class="btn btn-facebook btn-circle">
                  <i class="fa fa-refresh"></i>
                  </a>
              </li>
              <li class="list-inline-item">
                  <a href="#" class="btn btn-facebook btn-circle">
                  <i class="fa fa-facebook-f"></i>
                  </a>
              </li>
              <li class="list-inline-item">
                  <a href="#" class="btn btn-facebook btn-circle">
                  <i class="fa fa-linkedin"></i>
                  </a>
              </li>
              </ul>
          </div>
          <div class="col-md-4">
              <ul class="list-inline quicklinks">
              <li class="list-inline-item">
                  <a href="#">Privacy Policy</a>
              </li>
              <li class="list-inline-item">
                  <a href="#">Terms of Use</a>
              </li>
              </ul>
          </div>
      </div>
  </div>
</footer>