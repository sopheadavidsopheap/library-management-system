<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Events\Registered;
// use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request;

class Book extends Model
{
    public $fillable = [
        'name',
        'auth',
        'image',
    ];
    public static function addBook(Request $request){
        $books = new Book();
        foreach($books->fillable as $book){
            $books->{$book} = $request->get($book);
        }
        return $books->save();
        // dd($books);
    }
}
