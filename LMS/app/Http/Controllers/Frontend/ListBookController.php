<?php

namespace App\Http\Controllers\Frontend;

use App\Book;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ListBookController extends Controller
{
	public function index() {
		$data = Book::latest()->paginate(15);
		return view('welcome', compact('data'))
			->with('i', (request()->input('page', 1) - 1) * 5);
    }
}