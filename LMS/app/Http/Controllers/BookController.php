<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Request;

class BookController extends Controller
{
	public function index() {
		$data = Book::latest()->paginate(10);
		return view('layouts.pages.index', compact('data'))
			->with('i', (request()->input('page', 1) - 1) * 5);
    }
    
	public function create() {
		return view('layouts.pages.create');
	}
    
  public function store(Request $request) {
		$request->validate([
			'name' => 'required',
			'auth' => 'required',
			'image' => 'required|image|max:2048',
		]);

		$image = $request->file('image');

		$new_name = rand() . '.' . $image->getClientOriginalExtension();
		$image->move(public_path('images'), $new_name);
		$form_data = array(
			'name' => $request->name,
			'auth' => $request->auth,
			'image' => $new_name,
		);

		Book::create($form_data);
		// BooK::addBook($request);

		return redirect('book')->with('success', 'Data Added successfully.');
	}

	public function show($id) {
		$data = Book::findOrFail($id);
		return view('layouts.pages.view', compact('data'));
	}

	public function edit($id) {
		$data = Book::findOrFail($id);
		return view('layouts.pages.edit', compact('data'));
  }
    
	public function update(Request $request, $id) {
		$image_name = $request->hidden_image;
		$image = $request->file('image');
		if ($image != '') {
			$request->validate([
				'name' => 'required',
				'auth' => 'required',
				'image' => 'image|max:2048',
			]);
			$image_name = rand() . '.' . $image->getClientOriginalExtension();
			$image->move(public_path('images'), $image_name);
		} else {
			$request->validate([
				'name' => 'required',
				'auth' => 'required',
			]);
		}

		$form_data = array(
			'name' => $request->name,
			'auth' => $request->auth,
			'image' => $image_name,
		);

		Book::whereId($id)->update($form_data);
		return redirect('book')->with('success', 'Data is successfully updated');

    }
    
	public function destroy($id) {
		$data = Book::findOrFail($id);
		$path_image = public_path('/images/'.$data->image);
		if($path_image!=null){
			$data->delete();
			return redirect('book')->with('success', 'Data is successfully deleted');
		}
		else if($path_image==null){
			$data->delete();
			unlink($path_image);
			return redirect('book')->with('success', 'Data is successfully deleted');
		}else{
			return redirect('book')->with('success', 'Data is successfully deleted');
		}
	}
}
